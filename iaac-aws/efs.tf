resource "aws_efs_file_system" "main" {
  tags {
    Name = "Petclinic"
  }
}

resource "aws_efs_mount_target" "main" {
#  count = "1"

  file_system_id = "${aws_efs_file_system.main.id}"
  subnet_id      = "${aws_subnet.us-east-1a-private.id}"

  security_groups = [
    "${aws_security_group.efs.id}",
  ]
}

resource "aws_security_group" "efs" {
  name        = "efs-mnt"
  description = "Allows NFS traffic from instances within the VPC."
  vpc_id      = "${aws_vpc.default.id}"

  ingress {
    from_port = 2049
    to_port   = 2049
    protocol  = "tcp"

    cidr_blocks = [
      "${var.vpc_cidr}",
    ]
  }

  egress {
    from_port = 2049
    to_port   = 2049
    protocol  = "tcp"

    cidr_blocks = [
      "${var.vpc_cidr}",
    ]
  }

  tags {
    Name = "Petclinic - EFS SG"
  }
}
